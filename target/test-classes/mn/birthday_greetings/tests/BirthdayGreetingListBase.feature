Feature: Greeting employees birthday - by list
   
  Scenario: Greeting employees birthday by date
    The list should be printed in alphabetical order of the item names
        
      Given an employees list:
      | lastName | firstName   | birthDate      | email                  |
      | Doe       | John       |  1982/10/08    | john.doe@foobar.com    |
      | Ann       | Mary       |  1975/03/11    | mary.ann@foobar.com    |


    When I send messages greetings for birthday as "1982/10/08"
    Then the sended message list must be:
    
    |subject        | body                       |recipient|
    |Happy Birthday!| Happy Birthday, dear John! |john.doe@foobar.com|