package mn.birthday_greetings.employee_repository_adapters;

import java.util.List;

import mn.birthday_greetings.core.Employee;
import mn.birthday_greetings.core.EmployeeRepository;

public class ListEmployeeRepository implements EmployeeRepository{

	private final List<Employee> employees;

	public ListEmployeeRepository(List<Employee> employees) {
		this.employees = employees;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

}
