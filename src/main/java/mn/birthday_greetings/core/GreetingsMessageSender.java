package mn.birthday_greetings.core;

public interface GreetingsMessageSender {
	public void send(GreetingsMessage greetingsMessage);
}
