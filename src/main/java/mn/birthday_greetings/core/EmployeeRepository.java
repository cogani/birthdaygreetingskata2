package mn.birthday_greetings.core;

import java.util.List;

public interface EmployeeRepository {
	public List<Employee> getEmployees();
}
