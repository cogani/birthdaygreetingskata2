package mn.birthday_greetings.message_sender_adapters;

import java.util.List;
import java.util.ArrayList;

import javax.mail.Message;

import mn.birthday_greetings.core.GreetingsMessage;

public class FakeGreetingsEmailSender extends GreetingsEmailSender{
	private List<Message> messagesSent;
	private List<GreetingsMessage> greetingsMessagesSent;
	
	


	public FakeGreetingsEmailSender(String smtpHost, int smtpPort,
			String sender) {
		super(smtpHost, smtpPort, sender);
		this.messagesSent = new ArrayList<Message>();
		this.greetingsMessagesSent = new ArrayList<GreetingsMessage>();
	}
	
	public void send(GreetingsMessage greetingsMessage) 
	{		
		greetingsMessagesSent.add(greetingsMessage);
		messagesSent.add(this.prepareMessageReadyToSend(greetingsMessage));
	}
/*
 * TO REMOVE!!!
 * Creo que no está bien devolver la lista de Message enviados. Lo veo como muy a bajo nivel
 * En su lugar, creo que es más cercano al core devolver la lista de GreetingsMessages 
 
	public List<Message> getMessagesSent() {
		return messagesSent;
	}
	*/
	
	public List<GreetingsMessage> getGreetingsMessagesSent() {
		return greetingsMessagesSent;
	}
}
