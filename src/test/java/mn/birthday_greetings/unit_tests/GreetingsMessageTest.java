package mn.birthday_greetings.unit_tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;

import mn.birthday_greetings.core.Employee;
import mn.birthday_greetings.core.GreetingsMessage;

public class GreetingsMessageTest {
	Employee doeJohn;
	GreetingsMessage greetingDoeJhonBirthday;
	
	@Before
	public void setup() throws ParseException{
		doeJohn = new Employee("Doe", "John", "1982/10/08",
				"john.doe@foobar.com");
		greetingDoeJhonBirthday = new GreetingsMessage(doeJohn);
	}
	
	@Test
	public void isSameGreetingsMessage() throws ParseException{

		
		
		GreetingsMessage greetingDoeJhonBirthdayCopy = new GreetingsMessage(doeJohn);
		
		assertTrue(greetingDoeJhonBirthday.equals(greetingDoeJhonBirthdayCopy));
	}
	
	@Test
	public void isDifferentGreetingsMessage() throws ParseException{
	    
		Employee annMary = new Employee("Ann", "Mary", "1975/03/11",
				"mary.ann@foobar.com");
		
		GreetingsMessage greetingDoeJhonBirthday = new GreetingsMessage(doeJohn);
		GreetingsMessage greetingAnnMaryBirthdayCopy = new GreetingsMessage(annMary);
		
		assertFalse(greetingDoeJhonBirthday.equals(greetingAnnMaryBirthdayCopy));
	}

}
