package mn.birthday_greetings.tests;

import static org.junit.Assert.assertEquals;

import java.util.List;


import mn.birthday_greetings.core.BirthdayService;
import mn.birthday_greetings.core.Employee;
import mn.birthday_greetings.core.EmployeeRepository;
import mn.birthday_greetings.core.GreetingsMessage;
import mn.birthday_greetings.core.OurDate;
import mn.birthday_greetings.employee_repository_adapters.ListEmployeeRepository;
import mn.birthday_greetings.message_sender_adapters.FakeGreetingsEmailSender;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class BirthdayGreetingListBaseStepdefs {

	private BirthdayService service;
	private FakeGreetingsEmailSender greetingsMessageSender;

	@Given("^an employees list:$")
	public void a_shopping_list(List<Employee> employees) throws Throwable {
		/* For automatic conversion, change DataTable to List<YourType>
		 * Pending see how to use DataTable
		 */

		EmployeeRepository employeeRepository = new ListEmployeeRepository(employees);

		final String SMTP_HOST = "localhost";
		final int SMTP_PORT = 25;
		final String SENDER = "sender@here.com";
		
		greetingsMessageSender = new FakeGreetingsEmailSender(SMTP_HOST, SMTP_PORT, SENDER);

		service = new BirthdayService(greetingsMessageSender,
				employeeRepository);
	}

	@When("^I send messages greetings for birthday as \"([^\"]*)\"$")
	public void I_print_that_list(String birthday) throws Throwable {
		service.sendGreetings(new OurDate(birthday));
	}

	@Then("^the sended message list must be:$")
	public void it_should_look_like(List<GreetingsMessage> expectedMessageList)
			throws Throwable {
		List<GreetingsMessage> sendedMessageList = ((FakeGreetingsEmailSender) greetingsMessageSender)
				.getGreetingsMessagesSent();

		assertEquals(true, expectedMessageList.equals(sendedMessageList));
	}
}
