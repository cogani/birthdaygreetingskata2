package mn.birthday_greetings.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.text.ParseException;
import java.util.List;

import mn.birthday_greetings.core.BirthdayService;
import mn.birthday_greetings.core.Employee;
import mn.birthday_greetings.core.EmployeeRepository;
import mn.birthday_greetings.core.GreetingsMessage;
import mn.birthday_greetings.core.OurDate;
import mn.birthday_greetings.employee_repository_adapters.FileEmployeeRepository;
import mn.birthday_greetings.message_sender_adapters.FakeGreetingsEmailSender;
import mn.birthday_greetings.message_sender_adapters.GreetingsEmailSender;

// TO REMOVE
// Como ya no miro mensajes de correo sino greetingsMessage, esto ya no hace falta
//import javax.mail.Message;

public class BirthdayGreetingStepdefs {
	final int SMTP_PORT = 25;
	final String SENDER = "sender@here.com";
	String SMTP_HOST = "localhost";

	private BirthdayService service;
	private GreetingsEmailSender greetingsMessageSender;
	private EmployeeRepository employeeRepository;
	private OurDate birthDate, nobodyBirthdayDate;

	private GreetingsMessage greetingDoeJhonBirthday;

	@Before
	public void setup() throws ParseException {
		Employee doeJohn = new Employee("John", "Doe", "1982/10/08",
				"john.doe@foobar.com");
		greetingDoeJhonBirthday = new GreetingsMessage(doeJohn);

	}

	@Given("^an employees list \"([^\"]*)\"$")
	public void given_an_employees_list(String employeesDataFile)
			throws Throwable {
		greetingsMessageSender = new FakeGreetingsEmailSender(SMTP_HOST,
				SMTP_PORT, SENDER);

		/*
		 * 2 - Habria otra posiblidad de inyectarle al Fake un objeto lista para
		 * que el fake guardara ahi los mensajes enviados List<Message>
		 * messagesSent = new ArrayList(); greetingsMessageSender = new
		 * FakeGreetingsEmailSender(SMTP_HOST, SMTP_PORT, SENDER, messagesSent);
		 */
		employeeRepository = new FileEmployeeRepository(employeesDataFile);

		service = new BirthdayService(greetingsMessageSender,
				employeeRepository);
	}

	@When("^the employee birthday is \"([^\"]*)\"$")
	public void when_the_employee_birthday_is(String strDate) throws Throwable {
		birthDate = new OurDate(strDate);

	}

	@Then("^I expected to send (\\d+) greetings$")
	public void i_expected_to_send(int numbersGreetings) throws Throwable {

		service.sendGreetings(birthDate);
		/*
		 * 1 - Esto me parece raro: Como getMessagesSent solo está en subclase
		 * FakeGreetingsEmailSender, para que funcione tengo que hacer un
		 * downcasting a FakeGreetingsEmailSender Si el histórico de mensajes
		 * enviados se gestionara en GreetingsEmailSender, ya no haría falta
		 * este downcasting De esa forma siempre (en fake y no fake) estariamos
		 * guardando una copia de todos los mensajes enviados ¿eso tendría
		 * sentido?! 2 - Si hubiera escogido la opcion de inyectarle la
		 * instancia de lista (2-) ya no haría falta el getMessagesSent. Lo que
		 * pasa es que esto lo veo raro: la lista se comporta como parametro de
		 * entrada en el constructor, y despues lo estaríamos consultado para
		 * ver los mensajes enviados (con lo cual la lista tambien acturía como
		 * parametro de salida). Esto va en contra de lo que dice Clean Code
		 */
		List<GreetingsMessage> greetingMessagesSent = ((FakeGreetingsEmailSender) greetingsMessageSender)
				.getGreetingsMessagesSent();

		// Semanticamente esto lo veo bien
		assertEquals("message not sent?", numbersGreetings,
				greetingMessagesSent.size());

		// Pero, comprobar el contenido de los mensajes aquí y así ¿?!
		GreetingsMessage greetingSended = greetingMessagesSent.get(0);

		assertTrue(greetingDoeJhonBirthday.equals(greetingSended));

	}

	@When("^the date is \"([^\"]*)\"$")
	public void when_the_date_is(String strDate) throws Throwable {
		nobodyBirthdayDate = new OurDate(strDate);
	}

	@Then("^I expected to send no greetings$")
	public void then_I_expected_to_send_no_greetings() throws Exception {
		int numberExpectedSendedGreetings = 0;
		int numberSendedGreetings;

		// List<GreetingsMessage> expectedGreetingsMessage = new
		// ArrayList<GreetingsMessage>();

		service.sendGreetings(nobodyBirthdayDate);
		List<GreetingsMessage> greetingsMessagesSent = ((FakeGreetingsEmailSender) greetingsMessageSender)
				.getGreetingsMessagesSent();
		numberSendedGreetings = greetingsMessagesSent.size();

		assertEquals("what? messages?", numberExpectedSendedGreetings,
				numberSendedGreetings);
	}
}
