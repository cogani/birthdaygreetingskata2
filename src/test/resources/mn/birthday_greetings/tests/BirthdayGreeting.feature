Feature: Greeting employees birthday - by file name
    
  Scenario: Greeting employees by date
    Given an employees list "repositories/employee_data.txt"
    When the employee birthday is "1982/10/08" 
    Then I expected to send 1 greetings
    
  Scenario: Nobody birthday date    
    Given an employees list "repositories/employee_data.txt"
    When the date is "2008/01/01" 
    Then I expected to send no greetings